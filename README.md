# UniverSIS types

Generates the model type definitions from the universis-api

## Build

- Install dependencies with `npm install`
- Build the types with `npm run build`
- See the results at ./dist/types.d.ts

## Pack

- Generate the node package by running `npm pack`


## Features

This script clones the universis-api and parses the model definitions from the
models found in the `universis-api/server/config/models/*.json` files.

- __Model mapping__: It generates an interface for each model
- __Field type extraction__: The  field types are transformed to ts friendly types. If the type of the field is another model and the model is found in the models dir, that model is being used. Otherwise the `any` type is being applied.
- __Assossiations__: Maped models are being transformed to `Array<model>`
- __Inheritance__: Models with `inherits` or `implements` fields are being resolve recursively


## Modified api

If you're using a modified version of the api, you can use the `UNIVERSIS_TYPES_API_REPO` env variable to define the url of the api repo that you are using and the `UNIVERSIS_TYPES_API_DIR` variable to define the repo name that the repo will use (the repository's name).

e.g.
```shell
export UNIVERSIS_TYPES_API_REPO="https://gitlab.com/custom-universis-api.git"
export UNIVERSIS_TYPE_API_DIR="custom-universis-api"
npm run build
```
