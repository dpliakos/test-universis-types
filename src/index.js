const simpleGit = require('simple-git');
const path = require('path');
const fs = require('fs');
const {
  UNIVERSIS_API_REPO,
  UNIVERSIS_API_DIR,
  UNIVERSIS_API_MODELS_DIR
} = require('./constants');

let git = simpleGit({
  baseDir: process.cwd(),
  binary: 'git',
  maxConcurrentProcesses: 6,
});

/**
 * 
 * Initializes or updates the api repository
 * 
 * @returns {void}
 */
const setupApiRepo = async () => {
  const apiPath = path.resolve(process.cwd(), UNIVERSIS_API_DIR);
  const apiCloned = fs.existsSync(apiPath);

  if (!apiCloned) {
    console.log('Cloning universis-api repository from ', UNIVERSIS_API_REPO);
    await git.clone(UNIVERSIS_API_REPO);
  } else {
    git= simpleGit({
      baseDir: apiPath,
      binary: 'git',
      maxConcurrentProcesses: 6,
    });

    await git.pull(`origin`, 'master');
  }
}

/**
 * 
 * Reads all the model definitions
 * 
 * @returns {Array<string>} The list of all the models
 * 
 */
const getModels = () => {
  const modelsPath = path.resolve(process.cwd(), UNIVERSIS_API_DIR, UNIVERSIS_API_MODELS_DIR);
  const dirContents = fs.readdirSync(modelsPath);
  const models = [];
  for (let model of dirContents) {
    if (model.indexOf('.json') > 0) {
      models.push(model.split('.json')[0]);
    }
  }

  return models;
}

/**
 * 
 * Given a model name and it's fields, it generates the text of the model interface
 * 
 * @param {string} modelName The name of the model
 * @param {Array<object>} parsedFields The parsed definitions of the model's fields
 * @returns {string} The text representation of the interface
 */
const modelToInterface = (modelName, parsedFields) => {
  let intfc = `export declare interface ${modelName} {`;

  for (let field of parsedFields) {
    intfc += `\n\t${field.name}${field.required ? '' : '?'}: ${field.type};`;
  }

  intfc += `\n}`;

  return intfc;
}

/**
 * 
 * Given a model formatted for the @themost/data parser, generates the typescript
 * equivelant
 * 
 * @param {Array<string>} models The available model names 
 * @param {object} field The field definition
 * @returns {object} The parse field
 */
const getFieldType = (models, field) => {

  if (field.hasOwnProperty('mapping')) {
    if (models.includes(field.mapping.childModel)) {
      return `${field.mapping.childModel}[]`;
    } else {
      return 'any[]';
    }
  }

  switch (field.type) {
    case 'Integer':
    case 'Number':
    case 'Counter': return 'number';

    case 'Text':
    case 'String':
    case 'GUID':
    case 'URL': return 'string';

    case 'DateTime': return 'Date';

    case 'Boolean': return 'boolean';

    case 'Object': return 'any';

    default: return !models.includes(field.type)
      ? 'any'
      : `number | ${field.type}`;
  }
}

/**
 * 
 * Given a model formatted for the @themost/data parser, maps it's fields to
 * a typescript friendly format
 * @param {Array<string>} models The available model names 
 * @param {object} model The model definition
 * @returns {Array<object>} The parse fields
 */
const parseModelFields = (models, modelName) => {
  const modelPath = path.resolve(process.cwd(), UNIVERSIS_API_DIR, UNIVERSIS_API_MODELS_DIR, modelName + '.json');
  const modelDef = JSON.parse(fs.readFileSync(modelPath).toString());

  const modelFields = [];

  if (modelDef.hasOwnProperty('inherits') && models.includes(modelDef.inherits)) {
    const parentFields = parseModelFields(models, modelDef.inherits);
    modelFields.push(...parentFields);
  }

  if (modelDef.hasOwnProperty('implements') && models.includes(modelDef.implements)) {
    const parentFields = parseModelFields(models, modelDef.implements);
    modelFields.push(...parentFields);
  }

  for (let field of modelDef.fields) {

    // override existing declarations of the field
    for (let i = 0; i < modelFields.length; i++) {
      if (modelFields[i].name == field.name) {
        modelFields.splice(i, 1); // remove existing declaration
      }
    }

    modelFields.push({
      name: field.name,
      type: getFieldType(models, field),
      required: field.hasOwnProperty('nullable') && field.nullable === false
    });
  }

  return modelFields;
}

/**
 * 
 * Given the list of the available models, it generates the typescript interfaces for them
 * 
 * @param {Array<string>} models The available model names 
 * @returns {Array<string>} The list of the interfaces in text
 */
const generateInterfaces = (models) => {
  const allDefinitions = [];
  for (let modelName of models) {
    const modelFields = parseModelFields(models, modelName);
    const formattedInterface = modelToInterface(modelName, modelFields);
    allDefinitions.push(formattedInterface);
  }

  return allDefinitions;
}

setupApiRepo()
  .then(() => {
    const models = getModels();

    if (!models || !models.length || models.length == 0) {
      console.log('No models found');
      process.exit(0);
    } else {
      console.log('Found', models.length, 'models');
    }

    if (!fs.existsSync('dist')) {
      fs.mkdirSync('dist');
    }

    const formattedInterfaces = generateInterfaces(models);
    const targetFile = path.resolve(process.cwd(), 'dist', 'types.d.ts');
    fs.writeFileSync(targetFile, formattedInterfaces.join('\n\n') + '\n');
    console.log('Completed! You can find the interfaces at ', path.resolve(process.cwd(), 'dist', 'types.d.ts'));
    process.exit(0);
  })
  .catch(err => {
    console.error(err);
    process.exit(1);
  });
