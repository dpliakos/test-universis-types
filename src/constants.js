const envOrDefualt = (envVariable, literal) => {
  return process.env[envVariable] ? process.env[envVariable] : literal;
}

const UNIVERSIS_API_REPO = envOrDefualt('UNIVERSIS_TYPES_API_REPO', "https://gitlab.com/universis/universis-api.git");
const UNIVERSIS_API_DIR = envOrDefualt('UNIVERSIS_TYPES_API_DIR','universis-api');
const UNIVERSIS_API_MODELS_DIR = envOrDefualt('UNIVERSIS_TYPES_API_MODELS_DIR', 'server/config/models');

module.exports = {
  UNIVERSIS_API_REPO,
  UNIVERSIS_API_DIR,
  UNIVERSIS_API_MODELS_DIR
};
